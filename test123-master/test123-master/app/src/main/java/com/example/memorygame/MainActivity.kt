package com.example.memorygame

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_start.setOnClickListener{
            // Get the checked radio button id from radio group
            var id_size: Int = radio_group1.checkedRadioButtonId
            var id_time: Int = radio_group2.checkedRadioButtonId
            var size=0
            var time=0
            val intent = Intent(this,GameActivity::class.java)
            if (id_size!=-1&&id_time!=-1){
                size=when(id_size){
                    R.id.rbs2->2
                    R.id.rbs4->4
                    R.id.rbs6->6
                    else -> 0
                }
                time=when(id_time){
                    R.id.rbt0->999
                    R.id.rbt3->30
                    R.id.rbt6->60
                    else -> 0
                }
                intent.putExtra("time",time)
                intent.putExtra("size",size)
                startActivityForResult(intent, 1);
            }else{
                Toast.makeText(applicationContext,"你有選項沒有選到",
                    Toast.LENGTH_SHORT).show()
            }
        }
        fun radio_button_click(view: View){
            // Get the clicked radio button instance
            val radio: RadioButton = findViewById(radio_group1.checkedRadioButtonId)
            Toast.makeText(applicationContext,"On click : ${radio.text}",
                Toast.LENGTH_SHORT).show()
        }
    }
}
